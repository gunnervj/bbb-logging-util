package com.bbb.logging.util;

enum Unit { MILLISECOND, SECOND, MINUTES }
public record TimeLog(String id, Long timeTaken, Unit unit, String name,
                      String messageType, String messageSignature, String className,
                      Long startTimestamp, Long endTimestamp) {
}

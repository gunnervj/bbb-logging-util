package com.bbb.logging.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.time.Duration;
import java.time.Instant;

@Aspect
@Component
@Slf4j
@RequiredArgsConstructor
public class TimeLoggingAspect {
    private final ObjectMapper objectMapper;

    @Around("trackTime() && atExecution()")
    public Object logAroundTrackTime(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Instant start = Instant.now();
        Object result = proceedingJoinPoint.proceed();
        Instant end = Instant.now();
        long timeTaken = Duration.between(start, end).toMillis();
        MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();
        Method method = methodSignature.getMethod();
        TrackTime trackTime = method.getAnnotation(TrackTime.class);
        String key = CustomSpringExpressionLanguageParser.getDynamicValue(methodSignature.getParameterNames(),
                proceedingJoinPoint.getArgs(), trackTime.key());
        String messageType = CustomSpringExpressionLanguageParser.getDynamicValue(methodSignature.getParameterNames(),
                proceedingJoinPoint.getArgs(), trackTime.messageType());

        TimeLog timeLog = new TimeLog(key, timeTaken, Unit.MILLISECOND,trackTime.name(),
                messageType, methodSignature.getName(), methodSignature.getDeclaringTypeName(),
                start.toEpochMilli(), end.toEpochMilli());
        log.info(objectMapper.writeValueAsString(timeLog));
        return result;
    }

    @Pointcut("within(@com.bbb.logging.util.TrackTime * || @annotation(com.bbb.logging.util.TrackTime)")
    public void trackTime() {
        // point cut expression for aspectJ, no implementation required.
    }

    @Pointcut("execution(**(..))")
    public void atExecution() {
        // point cut expression for aspectJ, no implementation required.
    }
}

package com.bbb.logging.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CustomSpringExpressionLanguageParser {
    public static String getDynamicValue(String[] parameterNames, Object[] args, String key) {
        ExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext context = new StandardEvaluationContext();

        for (int i = 0; i< parameterNames.length; i++) {
          context.setVariable(parameterNames[i], args[i]);
        }

        if (StringUtils.isNoneBlank(key) && key.startsWith("#")) {
            return parser.parseExpression(key).getValue(context, String.class);
        }
        return key;
    }
}

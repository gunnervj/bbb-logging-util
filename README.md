## HOW TO USE TIME LOGGING ASPECT

In order to use the time logging aspect, you need to import the library to your project. 

After importing, if you want to log time taken for a method to execute, add annotation @TrackTime on top of the method declaration.

Example:

```java
@TrackTime(key="#payload.id", messageType = "#payload.messageType")
public void execute(Payload payload) {
   // business logic goes here
}
```

This will log the data in a json format to the log file. Tools such as newrelic could parse this information and can be 
used in the NRQL to create various dashboards.